# encoding: UTF-8
=begin
Copyright 2010-2012 Vincent Carmona
vinc4mai+taglib@gmail.com

This file is part of ruby-taglib2.

    ruby-taglib2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ruby-taglib2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ruby-taglib2; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'rake/rdoctask'
require 'rake/testtask'

compat=ENV['COMPAT']=="true"

#Default task
task :default => :build

#Building
file "Makefile" => ["extconf.rb"] do
	extconf="extconf.rb"
	extconf+=" --compat" if compat
	ruby(extconf)
end

desc "Build librairy"
task :build => "Makefile" do
  sh("make")
end

Rake::RDocTask.new do |rd|
	rd.options << "--exclude" << "extconf.rb"
	rd.options << "--exclude" << "tests/"
end

#Testing
Rake::TestTask.new do |t|
	t.libs=[]
	t.test_files=FileList['tests/tests.rb']
	t.options=" --compat" if compat
end

#Installing

desc "Install librairy"
task :install => :build do
	sh("make install")
end

#Cleaning
desc "Remove any temporary products."
task :clean do
  sh("make clean") if File.exist?("Makefile")
end

desc "Remove any generated file."
task :clobber  do
  sh("make distclean") if File.exist?("Makefile")
end

begin
	require 'rubygems'
	require 'rake/gempackagetask.rb'

	files=FileList['lib/**/*.rb', "compat/taglib.rb", '*.h', '*.c', 'extconf.rb',
		'tests/**/*',
		'ChangeLog', 'COPYING', 'README', 'Rakefile']
	ext=FileList["extconf.rb"]
	rdoc_options=["taglib2.c"]

	def retrieve_version
		File.open('lib/taglib2.rb'){|f| l=f.readlines}.
			grep(/VERSION=\[(\d+).*(\d+).*(\d+)\]/){$1+"."+$2+"."+$3}[0]
	end

	spec=Gem::Specification.new do |s|
		s.name='taglib2'
		s.version=retrieve_version
		s.summary="ruby-taglib2 is a Ruby interface to TagLib, the audio meta-data library. It allows Ruby programs to read and write meta-data of all the audio formats supported by TagLib. It was written because ruby-taglib suffers severals bugs with ruby 1.9."
		s.author="Vincent Carmona"
		s.email="vinc4mai@gmail.com"
		s.rubyforge_project='zik'
		s.homepage="http://zik.rubyforge.org/ruby-taglib2"
		s.license="General Public License v2"

		s.files=files.to_a
		s.extensions=ext.to_a

		s.rdoc_options=rdoc_options

		if ENV['GEM_PRIVATE_KEY']&&ENV['GEM_CERTIFICATE_CHAIN']
			s.signing_key=ENV['GEM_PRIVATE_KEY']
			s.cert_chain=ENV['GEM_CERTIFICATE_CHAIN'].split(" ")
		else
			$stderr.puts "WARNING: gem will not be signed."
		end
	end
	Rake::GemPackageTask.new(spec){}
rescue LoadError => e
	$stderr.puts "WARNING: "+e
end
