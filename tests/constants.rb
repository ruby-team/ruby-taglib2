=begin
Copyright 2010-2012 Vincent Carmona
vinc4mai+taglib@gmail.com

This file is part of ruby-taglib2.

    ruby-taglib2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ruby-taglib2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ruby-taglib2; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

class ConstantsTest < MiniTest::Unit::TestCase
	def test_mpeg
		assert_instance_of(Fixnum, TagLib::MPEG)
	end

	def test_oggvorbis
		assert_instance_of(Fixnum, TagLib::OggVorbis)
	end

	def test_flac
		assert_instance_of(Fixnum, TagLib::FLAC)
	end

	def test_mpc
		assert_instance_of(Fixnum, TagLib::MPC)
	end

	def test_oggflac
		assert_instance_of(Fixnum, TagLib::OggFlac)
	end

	def test_wavpack
		assert_instance_of(Fixnum, TagLib::WavPack)
	end

	def test_speex
		assert_instance_of(Fixnum, TagLib::Speex)
	end

	def test_trueaudio
		assert_instance_of(Fixnum, TagLib::TrueAudio)
	end

	def test_mp4
		assert_instance_of(Fixnum, TagLib::MP4)
	end

	def test_asf
		assert_instance_of(Fixnum, TagLib::ASF)
	end
end
