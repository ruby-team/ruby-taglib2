=begin
Copyright 2010-2012 Vincent Carmona
vinc4mai+taglib@gmail.com

This file is part of ruby-taglib2.

    ruby-taglib2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ruby-taglib2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ruby-taglib2; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

class TagLibFileTest < MiniTest::Unit::TestCase
	dir=File.expand_path(File.dirname(__FILE__))
	tmp=File.join(dir, 'tmp')
	data=File.join(dir, 'data')
	READ=File.join(tmp, 'read.wav')
	EMPTY=File.join(tmp, 'empty')
	WRITE=File.join(tmp, 'write.wav')
	EXCEPTIONS=File.join(tmp, 'exceptions.ogg')

	FileUtils.mkdir(tmp)
	FileUtils.copy(File.join(data, 'silence.wav'), READ)
	FileUtils.copy(File.join(data, 'empty'), EMPTY)
	FileUtils.copy(File.join(data, 'silence.wav'), WRITE)
	FileUtils.copy(File.join(data, 'exceptions.ogg'), EXCEPTIONS)	

	MiniTest::Unit.after_tests{FileUtils.remove_entry_secure(tmp)}

	def test_new
		assert_instance_of(TagLib::File, TagLib::File.new(READ))
	end

	def test_new2
		assert_instance_of(TagLib::File, TagLib::File.new(READ, TagLib::OggVorbis))
	end

	def test_badfile
		assert_raises(TagLib::BadFile){TagLib::File.new(EMPTY)}
	end

	def test_badtag
		assert_raises(TagLib::BadTag){
			f=TagLib::File.new(EXCEPTIONS)
			f.title
		}
	end

	def test_badaudio
		assert_raises(TagLib::BadAudioProperties){
			f=TagLib::File.new(EXCEPTIONS)
			f.length
		}
	end

	def check_tag(file, tag)
		assert_equal(tag[:title], file.title)
		assert_equal(tag[:artist], file.artist)
		assert_equal(tag[:album], file.album)
		assert_equal(tag[:comment], file.comment)
		assert_equal(tag[:genre], file.genre)
		assert_equal(tag[:year], file.year)
		assert_equal(tag[:track], file.track)
	end


	def test_readtag
		f=TagLib::File.new(READ)
		check_tag(f, {:title=>'Title', :artist=>'Artist', :album=>'Album',
			:comment=>'Comment', :genre=>'Genre', :year=>2010, :track=>12})
		assert_equal(3, f.length)
		assert_equal(64, f.bitrate)
		assert_equal(8000, f.samplerate)
		assert_equal(1, f.channels)
	end

	def test_write
		f=TagLib::File.new(WRITE)
		assert_instance_of(TagLib::File, f)
		f.title="title"
		f.artist="artist"
		f.album="album"
		f.comment="comment"
		f.genre="genre"
		f.year=1020
		f.track=21
		assert_equal(true, f.save)
		check_tag(TagLib::File.new(WRITE), {:title=>'title', :artist=>'artist',
			:album=>'album', :comment=>'comment', :genre=>'genre',
			:year=>1020, :track=>21})
	end

	def test_close
		f=TagLib::File.new(READ)
		assert_equal(f, f.close)
	end
end
