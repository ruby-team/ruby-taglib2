=begin
Copyright 2010-2012 Vincent Carmona
vinc4mai+taglib@gmail.com

This file is part of ruby-taglib2.

    ruby-taglib2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ruby-taglib2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ruby-taglib2; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
=end

require 'fileutils'
require 'minitest/autorun'

compat=false
if ARGV.include?('--compat')
	ARGV.delete('--compat')
	compat=true
end

dir=File.expand_path(File.join(File.dirname(__FILE__), ".."))
$:.unshift(dir)
$:.unshift(File.join(dir, 'lib'))
if compat
	$:.unshift(File.join(dir, 'compat'))
	require 'taglib'
else
	require "taglib2"
end
puts "taglib2 version #{TagLib::VERSION.join('.')}"

tests=%w(constants exceptions file)
tests << "compat" if compat
if Kernel.respond_to? :require_relative
	tests.each{|test| require_relative test}
else#DEPRECATED: ruby1.8
	tests.each{|test| require File.join(dir, 'tests', test)}
end
